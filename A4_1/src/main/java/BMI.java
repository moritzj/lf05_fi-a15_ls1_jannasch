import java.util.Scanner;

public class BMI {
    public static void main(String[] args) {
        char sex;
        float height;
        float weight;
        float bmi;

        Scanner input = new Scanner(System.in);

        System.out.println("Herzlich willkommen zum BMI-Rechner!\nBitte geben Sie ihr Geschlecht ein (m/w): ");
        sex = input.next().charAt(0);

        System.out.println("Bitte geben Sie jetzt Ihre Körpergröße in Meter ein: ");
        height = input.nextFloat();

        System.out.println("Zum Schluss geben Sie bitte Ihr Körpergewicht in Kilogramm ein: ");
        weight = input.nextFloat();

        bmi = weight / (height * height);

        System.out.printf("Ihr BMI ist %.2f, ", bmi);

        if (sex == 'm' && bmi < 20 || sex == 'w' && bmi < 19) {
            System.out.println("damit sind Sie untergewichtig.");
        } else if (sex == 'm' && bmi >= 20 && bmi <= 25 || sex == 'w' && bmi >= 19 && bmi <= 24) {
            System.out.println("damit sind Sie normalgewichtig.");
        } else if (sex == 'm' && bmi > 25 || sex == 'w' && bmi > 24) {
            System.out.println("damit sind Sie übergewichtig.");
        }

    }
}
