import java.util.Scanner;

public class Sortiere_abc {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        //String ausgabe;

        System.out.println("Geben sie drei Zeichen ein: ");
        String eingabe = input.next();
        char char1 = eingabe.charAt(0);
        char char2 = eingabe.charAt(1);
        char char3 = eingabe.charAt(2);

        int ascii1 = (int) char1;
        int ascii2 = (int) char2;
        int ascii3 = (int) char3;

        int ausgabe3 = ascii1 > ascii2 && ascii1 > ascii3 ? ascii1 : ascii2 > ascii1 && ascii2 > ascii3 ? ascii2 : ascii3;
        int ausgabe1 = ascii1 < ascii2 && ascii1 < ascii3 ? ascii1 : ascii2 < ascii1 && ascii2 < ascii3 ? ascii2 : ascii3;
        int ausgabe2 = ascii1 != ausgabe3 && ascii1 != ausgabe1 ? ascii1 : ascii2 != ausgabe3 && ascii2 != ausgabe1 ? ascii2 : ascii3;

        //ausgabe = (char) ausgabe1 + (char) ausgabe2 + (char) ausgabe3 + "";
        //ausgabe = ((char) ausgabe1) + ((char) ausgabe2) + ((char) ausgabe3) + "";
        //ausgabe = String.valueOf((char) ausgabe1 + (char) ausgabe2 + (char) ausgabe3);
        //ausgabe = String.valueOf((char) ausgabe1) + String.valueOf((char) ausgabe2) + String.valueOf((char) ausgabe3);

        //System.out.println("Sortiert: " + ausgabe);

        //Nicht fragen, das andere wirkt logisch, aber Java sagt nein...
        System.out.print("Sortiert: ");
        System.out.print((char) ausgabe1);
        System.out.print((char) ausgabe2);
        System.out.print((char) ausgabe3);

    }
}
