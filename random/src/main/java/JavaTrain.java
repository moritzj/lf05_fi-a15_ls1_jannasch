import java.util.Scanner;

public class JavaTrain {
    public static void main(String[] args) {
        int fahrzeit = 0;
        char haltInSpandau;
        char richtungHamburg;
        char haltInStendal;
        char endetIn;

        Scanner input = new Scanner(System.in);

        System.out.println("Soll der Zug in Spandau halten (j/n)?: ");
        haltInSpandau = input.next().charAt(0);

        fahrzeit = fahrzeit + 8; // Fahrzeit: Berlin Hbf -> Spandau

        if (haltInSpandau == 'j') {
            fahrzeit = fahrzeit + 2; // Halt in Spandau
        }

        System.out.println("Zweigt der Zug nach Hamburg ab (j/n)?: ");
        richtungHamburg = input.next().charAt(0);

        if (richtungHamburg == 'j') {
            fahrzeit = fahrzeit + 96;
            System.out.printf("Sie erreichen Hamburg nach %d Minuten.", fahrzeit);
        } else {
            fahrzeit = fahrzeit + 34;

            System.out.println("Soll der Zug in Stendal halten (j/n)?: ");
            haltInStendal = input.next().charAt(0);

            if (haltInStendal == 'j') {
                fahrzeit = fahrzeit + 16;
            } else {
                fahrzeit = fahrzeit +6;
            }

            System.out.println("Bis wohin fährt der Zug (w/b/h)?: ");
            endetIn = input.next().charAt(0);

            if (endetIn == 'w') {
                fahrzeit = fahrzeit + 29;
                System.out.printf("Sie erreichen Wolfsburg nach %d Minuten.", fahrzeit);
            } else if (endetIn == 'b') {
                fahrzeit = fahrzeit + 50;
                System.out.printf("Sie erreichen Braunschweig nach %d Minuten.", fahrzeit);
            } else {
                fahrzeit = fahrzeit + 62;
                System.out.printf("Sie erreichen Hannover nach %d Minuten.", fahrzeit);
            }

        }

    }

}
